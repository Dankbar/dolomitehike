Komplette Wanderung - Variante A:
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-gesamte-wanderung-variante-a/259542716/?share=%7Ezvohwwak%244osswmns

Bild Variante A; handgezeichnet in grün mit Variante B
![Hike Map](whole_hike.png "Bild Variante A; handgezeichnet in grün mit Variante B")

## Plan A:
### Tag 1: Anreise nach Rifugio Zannes (4h52m mit aktueller Verbindung), Anstieg zur Schlüterhütte 4.4km, 2h, 630up, 2down
- Optional: Besteigung des Peitlerkofel 2h up, 1h down, 600up/down; ganz leichter (A) Klettersteig ganz am Ende
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-1/259448918/
#### Tag 2A: Schlüterhütte - Puezhütte 9.8km, 5h-6h, 750up, 550down
- wahrscheinlich Abstecher zum Piz Duleda (60 hm)
- von der Hütte optional Puezspitze und Piza de Puez (500h, 2,5h hin- und zurück)
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-2-schlueterhuette-puezhuette/259449267/?share=%7Ezuzoywfe%244ossweig
#### Tag 3/4: Puezhütte - Pisciaduhütte via Pisciaduklettersteig 10.5km, 5-6h, 875up, 750 down
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag3-puezhuette-pisciaduhuette/259449850/?share=%7Ezuzok3jv%244ossweik
- Klettersteig mit C Stellen. Vor den C-Stellen ist ein Notausstieg, von dem aus man hochwandern kann. Die Stelle ist davor sichtbar, um sie einschätzen zu können (https://www.bergsteigen.com/touren/klettersteig/pisciadu-klettersteig-sella-gruppe/)
- am Grödner Joch könnten wir noch einfache, kurze Klettersteige auf die Cirspitzen machen, wenn wir schnell sind. Topo hat beide Klettersteige: https://www.bergsteigen.com/touren/klettersteig/kleine-cirspitze-klettersteig/
#### Tag 4A: Dolomiten Tag 4: Pisciaduhütte - Lichtenfelser Weg bis fast Franz-Kostner-Hütte - Capanna Piz Fassa (9.2km, 7h?, 1250up, 700down)
- sehr großer Umweg zur Hütte, da sonst sehr kurz (3h30)
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-4a-pisciaduhuette-lichtenfelser-weg-capanna-piz-fassa/259451976/?share=%7Ezuzpev4o%244osswecg
- Schlenker ist größtenteils das hier (https://www.tourentagebuch.de/auf-dem-lichtenfelser-steig-zum-piz-boe/) oder das hier (https://www.youtube.com/watch?v=1d9NgEPjZ78)
- Könnten an dem Tag auch andere Schlenker auf der Sella machen, um 4-5h zu wandern. Oder flexibler kurzer/langer Tag. Können uns auch aufteilen.
#### Tag 5A: Rifugio Capanna Piz Fassa - Langkofelhütte (5h, 13km, 829up, 1723down)
- kann mit Seilbahn verkürzt werden (450hm up fallen weg); Stehgondel; 20€! https://www.suedtirol-kompakt.com/mit-der-gondelbahn-auf-die-langkofelscharte/
- können 200hm sparen und mehr Wiese/weniger Stein haben, indem wir Langkofel nördlich umrunden. 30min mehr
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-5a-rifugio-capanna-fassa-langkofelhuette/259497703/?share=%7Ezvoenvja%244osswfcg
#### Tag 6: Dolomiten Tag 6: Langkofelhütte - Plattkofelüberschreitung - Antermoiahütte 13.5km, 6.5h, 1332up, 1104down
- Oskar-Schuster-Klettersteig über den Plattkofel B/C: https://www.bergsteigen.com/touren/klettersteig/oskar-schuster-klettersteig-langkofel/
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-6-langkofelhuette-plattkofelueberschreitung-antermoiahuette/259499089/?share=%7Ezvofiobm%244osswfcr
#### Tag 7: Rifugio Antermoia - Kesselkogel - Schlernhaus 13km, 6.5h, 1200up, 1260down
- Kesselkogelüberschreitung Klettersteig B (https://www.via-ferrata.de/klettersteige/topo/kesselkogel-und-cima-scalieret)
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-7-rifugio-antermoia-kesselkogel-schlernhaus/259499374/?share=%7Ezvof4cve%244osswfct
#### Tag 8: Abstiegn nach Seis am Schlern 3.5h, 1500down; Heimfahrt (4h45 bis München HBF)
- könnten auch auf die Seiser Alm steigen und mit der Gondel abfahren

## Alternative Möglichkeit B:
#### Tag 2B: Schlüterhütte - Sass Rigais (Geislerspitzen) Überschreitung - Regensburger Hütte 14.5km, 7h, 1300up, 1600down
- Sass Rigais Überschreitung Klettersteig B/C: https://www.bergsteigen.com/touren/klettersteig/sass-rigais-ueberschreitung/
- ohne Klettersteig 4 Std und deutlich weniger Höhenmeter; könnten dann noch zur/Richtung Seceda wandern
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-2b-schlueterhuette-sass-rigais-regensburger-huette/259500611/?share=%7Ezvofae4z%244osswkru
#### Tag 3B: Regensburger Hütte - Col da la Pieres - Puezhütte 5h 9.6km, 1000up, 550down
- für Sidepeaks an Puezhütte, siehe Tag 2A
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-3b-regensburger-huette-col-da-la-pieres-puezhuette/259500783/?share=%7Ezvofeigh%244osswkrv
#### Tag 5B: Pisciaduhütte - Sellahochfläche - Langkofelhütte (5h30, 14km, 1100up 1500down)
- sehr ähnlich Tag 5A. Könnten auch hier Seilbahn nehmen oder die Langkofelscharte umgehen
- https://www.alpenvereinaktiv.com/de/tour/dolomiten-tag-5b-pisciaduhuette-sellahochflaeche-langkofelhuette/259500306/?share=%7Ezvofpbxh%244osswkrr